<?php
/**
 * This file aims to show you how to use this generated package.
 * In addition, the goal is to show which methods are available and the first needed parameter(s)
 * You have to use an associative array such as:
 * - the key must be a constant beginning with WSDL_ from AbstractSoapClientBase class (each generated ServiceType class extends this class)
 * - the value must be the corresponding key value (each option matches a {@link http://www.php.net/manual/en/soapclient.soapclient.php} option)
 * $options = [
 * WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_URL => 'https://wms.peoplevox.net/egs3398/resources/integrationservicev4.asmx?wsdl',
 * WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_TRACE => true,
 * WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_LOGIN => 'you_secret_login',
 * WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_PASSWORD => 'you_secret_password',
 * ];
 * etc...
 */
require_once __DIR__ . '/vendor/autoload.php';
/**
 * Minimal options
 */
$options = [
    WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_URL => 'https://wms.peoplevox.net/egs3398/resources/integrationservicev4.asmx?wsdl',
    WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_CLASSMAP => ClassMap::get(),
];
/**
 * Samples for Get ServiceType
 */
$get = new \ServiceType\Get($options);
$get->setSoapHeaderUserSessionCredentials(new \StructType\UserSessionCredentials());
/**
 * Sample call for GetData operation/method
 */
if ($get->GetData(new \StructType\GetData()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetSystemSettings operation/method
 */
if ($get->GetSystemSettings(new \StructType\GetSystemSettings()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetReportData operation/method
 */
if ($get->GetReportData(new \StructType\GetReportData()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetReportColumns operation/method
 */
if ($get->GetReportColumns(new \StructType\GetReportColumns()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetSaveTemplate operation/method
 */
if ($get->GetSaveTemplate(new \StructType\GetSaveTemplate()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Samples for Save ServiceType
 */
$save = new \ServiceType\Save($options);
$save->setSoapHeaderUserSessionCredentials(new \StructType\UserSessionCredentials());
/**
 * Sample call for SaveData operation/method
 */
if ($save->SaveData(new \StructType\SaveData()) !== false) {
    print_r($save->getResult());
} else {
    print_r($save->getLastError());
}
/**
 * Samples for Subscribe ServiceType
 */
$subscribe = new \ServiceType\Subscribe($options);
$subscribe->setSoapHeaderUserSessionCredentials(new \StructType\UserSessionCredentials());
/**
 * Sample call for SubscribeEvent operation/method
 */
if ($subscribe->SubscribeEvent(new \StructType\SubscribeEvent()) !== false) {
    print_r($subscribe->getResult());
} else {
    print_r($subscribe->getLastError());
}
/**
 * Sample call for SubscribeEventWithSitesFilters operation/method
 */
if ($subscribe->SubscribeEventWithSitesFilters(new \StructType\SubscribeEventWithSitesFilters()) !== false) {
    print_r($subscribe->getResult());
} else {
    print_r($subscribe->getLastError());
}
/**
 * Sample call for SubscribePostEvent operation/method
 */
if ($subscribe->SubscribePostEvent(new \StructType\SubscribePostEvent()) !== false) {
    print_r($subscribe->getResult());
} else {
    print_r($subscribe->getLastError());
}
/**
 * Samples for Unsubscribe ServiceType
 */
$unsubscribe = new \ServiceType\Unsubscribe($options);
$unsubscribe->setSoapHeaderUserSessionCredentials(new \StructType\UserSessionCredentials());
/**
 * Sample call for UnsubscribeEvent operation/method
 */
if ($unsubscribe->UnsubscribeEvent(new \StructType\UnsubscribeEvent()) !== false) {
    print_r($unsubscribe->getResult());
} else {
    print_r($unsubscribe->getLastError());
}
/**
 * Samples for Authenticate ServiceType
 */
$authenticate = new \ServiceType\Authenticate($options);
/**
 * Sample call for Authenticate operation/method
 */
if ($authenticate->Authenticate(new \StructType\Authenticate()) !== false) {
    print_r($authenticate->getResult());
} else {
    print_r($authenticate->getLastError());
}
