<?php

declare(strict_types=1);

namespace ArrayType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfCarrierConnectionForService ArrayType
 * @subpackage Arrays
 */
class ArrayOfCarrierConnectionForService extends AbstractStructArrayBase
{
    /**
     * The CarrierConnection
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \StructType\CarrierConnectionForService[]
     */
    protected array $CarrierConnection = [];
    /**
     * Constructor method for ArrayOfCarrierConnectionForService
     * @uses ArrayOfCarrierConnectionForService::setCarrierConnection()
     * @param \StructType\CarrierConnectionForService[] $carrierConnection
     */
    public function __construct(array $carrierConnection = [])
    {
        $this
            ->setCarrierConnection($carrierConnection);
    }
    /**
     * Get CarrierConnection value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \StructType\CarrierConnectionForService[]
     */
    public function getCarrierConnection(): ?array
    {
        return isset($this->CarrierConnection) ? $this->CarrierConnection : null;
    }
    /**
     * This method is responsible for validating the values passed to the setCarrierConnection method
     * This method is willingly generated in order to preserve the one-line inline validation within the setCarrierConnection method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateCarrierConnectionForArrayConstraintsFromSetCarrierConnection(array $values = []): string
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfCarrierConnectionForServiceCarrierConnectionItem) {
            // validation for constraint: itemType
            if (!$arrayOfCarrierConnectionForServiceCarrierConnectionItem instanceof \StructType\CarrierConnectionForService) {
                $invalidValues[] = is_object($arrayOfCarrierConnectionForServiceCarrierConnectionItem) ? get_class($arrayOfCarrierConnectionForServiceCarrierConnectionItem) : sprintf('%s(%s)', gettype($arrayOfCarrierConnectionForServiceCarrierConnectionItem), var_export($arrayOfCarrierConnectionForServiceCarrierConnectionItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The CarrierConnection property can only contain items of type \StructType\CarrierConnectionForService, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set CarrierConnection value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \StructType\CarrierConnectionForService[] $carrierConnection
     * @return \ArrayType\ArrayOfCarrierConnectionForService
     */
    public function setCarrierConnection(array $carrierConnection = []): self
    {
        // validation for constraint: array
        if ('' !== ($carrierConnectionArrayErrorMessage = self::validateCarrierConnectionForArrayConstraintsFromSetCarrierConnection($carrierConnection))) {
            throw new InvalidArgumentException($carrierConnectionArrayErrorMessage, __LINE__);
        }
        if (is_null($carrierConnection) || (is_array($carrierConnection) && empty($carrierConnection))) {
            unset($this->CarrierConnection);
        } else {
            $this->CarrierConnection = $carrierConnection;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \StructType\CarrierConnectionForService|null
     */
    public function current(): ?\StructType\CarrierConnectionForService
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \StructType\CarrierConnectionForService|null
     */
    public function item($index): ?\StructType\CarrierConnectionForService
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \StructType\CarrierConnectionForService|null
     */
    public function first(): ?\StructType\CarrierConnectionForService
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \StructType\CarrierConnectionForService|null
     */
    public function last(): ?\StructType\CarrierConnectionForService
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \StructType\CarrierConnectionForService|null
     */
    public function offsetGet($offset): ?\StructType\CarrierConnectionForService
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \StructType\CarrierConnectionForService $item
     * @return \ArrayType\ArrayOfCarrierConnectionForService
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \StructType\CarrierConnectionForService) {
            throw new InvalidArgumentException(sprintf('The CarrierConnection property can only contain items of type \StructType\CarrierConnectionForService, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string CarrierConnection
     */
    public function getAttributeName(): string
    {
        return 'CarrierConnection';
    }
}
