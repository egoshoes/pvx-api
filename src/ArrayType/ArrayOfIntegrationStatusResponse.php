<?php

declare(strict_types=1);

namespace ArrayType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfIntegrationStatusResponse ArrayType
 * @subpackage Arrays
 */
class ArrayOfIntegrationStatusResponse extends AbstractStructArrayBase
{
    /**
     * The IntegrationStatusResponse
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \StructType\IntegrationStatusResponse[]
     */
    protected array $IntegrationStatusResponse = [];
    /**
     * Constructor method for ArrayOfIntegrationStatusResponse
     * @uses ArrayOfIntegrationStatusResponse::setIntegrationStatusResponse()
     * @param \StructType\IntegrationStatusResponse[] $integrationStatusResponse
     */
    public function __construct(array $integrationStatusResponse = [])
    {
        $this
            ->setIntegrationStatusResponse($integrationStatusResponse);
    }
    /**
     * Get IntegrationStatusResponse value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \StructType\IntegrationStatusResponse[]
     */
    public function getIntegrationStatusResponse(): ?array
    {
        return isset($this->IntegrationStatusResponse) ? $this->IntegrationStatusResponse : null;
    }
    /**
     * This method is responsible for validating the values passed to the setIntegrationStatusResponse method
     * This method is willingly generated in order to preserve the one-line inline validation within the setIntegrationStatusResponse method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateIntegrationStatusResponseForArrayConstraintsFromSetIntegrationStatusResponse(array $values = []): string
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfIntegrationStatusResponseIntegrationStatusResponseItem) {
            // validation for constraint: itemType
            if (!$arrayOfIntegrationStatusResponseIntegrationStatusResponseItem instanceof \StructType\IntegrationStatusResponse) {
                $invalidValues[] = is_object($arrayOfIntegrationStatusResponseIntegrationStatusResponseItem) ? get_class($arrayOfIntegrationStatusResponseIntegrationStatusResponseItem) : sprintf('%s(%s)', gettype($arrayOfIntegrationStatusResponseIntegrationStatusResponseItem), var_export($arrayOfIntegrationStatusResponseIntegrationStatusResponseItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The IntegrationStatusResponse property can only contain items of type \StructType\IntegrationStatusResponse, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set IntegrationStatusResponse value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \StructType\IntegrationStatusResponse[] $integrationStatusResponse
     * @return \ArrayType\ArrayOfIntegrationStatusResponse
     */
    public function setIntegrationStatusResponse(array $integrationStatusResponse = []): self
    {
        // validation for constraint: array
        if ('' !== ($integrationStatusResponseArrayErrorMessage = self::validateIntegrationStatusResponseForArrayConstraintsFromSetIntegrationStatusResponse($integrationStatusResponse))) {
            throw new InvalidArgumentException($integrationStatusResponseArrayErrorMessage, __LINE__);
        }
        if (is_null($integrationStatusResponse) || (is_array($integrationStatusResponse) && empty($integrationStatusResponse))) {
            unset($this->IntegrationStatusResponse);
        } else {
            $this->IntegrationStatusResponse = $integrationStatusResponse;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \StructType\IntegrationStatusResponse|null
     */
    public function current(): ?\StructType\IntegrationStatusResponse
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \StructType\IntegrationStatusResponse|null
     */
    public function item($index): ?\StructType\IntegrationStatusResponse
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \StructType\IntegrationStatusResponse|null
     */
    public function first(): ?\StructType\IntegrationStatusResponse
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \StructType\IntegrationStatusResponse|null
     */
    public function last(): ?\StructType\IntegrationStatusResponse
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \StructType\IntegrationStatusResponse|null
     */
    public function offsetGet($offset): ?\StructType\IntegrationStatusResponse
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \StructType\IntegrationStatusResponse $item
     * @return \ArrayType\ArrayOfIntegrationStatusResponse
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \StructType\IntegrationStatusResponse) {
            throw new InvalidArgumentException(sprintf('The IntegrationStatusResponse property can only contain items of type \StructType\IntegrationStatusResponse, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string IntegrationStatusResponse
     */
    public function getAttributeName(): string
    {
        return 'IntegrationStatusResponse';
    }
}
