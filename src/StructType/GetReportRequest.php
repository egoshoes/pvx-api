<?php

declare(strict_types=1);

namespace StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetReportRequest StructType
 * @subpackage Structs
 */
class GetReportRequest extends GetRequest
{
    /**
     * The FilterClause
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $FilterClause = null;
    /**
     * The OrderBy
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $OrderBy = null;
    /**
     * The Columns
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $Columns = null;
    /**
     * Constructor method for GetReportRequest
     * @uses GetReportRequest::setFilterClause()
     * @uses GetReportRequest::setOrderBy()
     * @uses GetReportRequest::setColumns()
     * @param string $filterClause
     * @param string $orderBy
     * @param string $columns
     */
    public function __construct(?string $filterClause = null, ?string $orderBy = null, ?string $columns = null)
    {
        $this
            ->setFilterClause($filterClause)
            ->setOrderBy($orderBy)
            ->setColumns($columns);
    }
    /**
     * Get FilterClause value
     * @return string|null
     */
    public function getFilterClause(): ?string
    {
        return $this->FilterClause;
    }
    /**
     * Set FilterClause value
     * @param string $filterClause
     * @return \StructType\GetReportRequest
     */
    public function setFilterClause(?string $filterClause = null): self
    {
        // validation for constraint: string
        if (!is_null($filterClause) && !is_string($filterClause)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($filterClause, true), gettype($filterClause)), __LINE__);
        }
        $this->FilterClause = $filterClause;
        
        return $this;
    }
    /**
     * Get OrderBy value
     * @return string|null
     */
    public function getOrderBy(): ?string
    {
        return $this->OrderBy;
    }
    /**
     * Set OrderBy value
     * @param string $orderBy
     * @return \StructType\GetReportRequest
     */
    public function setOrderBy(?string $orderBy = null): self
    {
        // validation for constraint: string
        if (!is_null($orderBy) && !is_string($orderBy)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($orderBy, true), gettype($orderBy)), __LINE__);
        }
        $this->OrderBy = $orderBy;
        
        return $this;
    }
    /**
     * Get Columns value
     * @return string|null
     */
    public function getColumns(): ?string
    {
        return $this->Columns;
    }
    /**
     * Set Columns value
     * @param string $columns
     * @return \StructType\GetReportRequest
     */
    public function setColumns(?string $columns = null): self
    {
        // validation for constraint: string
        if (!is_null($columns) && !is_string($columns)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($columns, true), gettype($columns)), __LINE__);
        }
        $this->Columns = $columns;
        
        return $this;
    }
}
