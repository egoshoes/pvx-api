<?php

declare(strict_types=1);

namespace StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetReportColumnsResponse StructType
 * @subpackage Structs
 */
class GetReportColumnsResponse extends AbstractStructBase
{
    /**
     * The GetReportColumnsResult
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \StructType\IntegrationResponse|null
     */
    protected ?\StructType\IntegrationResponse $GetReportColumnsResult = null;
    /**
     * Constructor method for GetReportColumnsResponse
     * @uses GetReportColumnsResponse::setGetReportColumnsResult()
     * @param \StructType\IntegrationResponse $getReportColumnsResult
     */
    public function __construct(?\StructType\IntegrationResponse $getReportColumnsResult = null)
    {
        $this
            ->setGetReportColumnsResult($getReportColumnsResult);
    }
    /**
     * Get GetReportColumnsResult value
     * @return \StructType\IntegrationResponse|null
     */
    public function getGetReportColumnsResult(): ?\StructType\IntegrationResponse
    {
        return $this->GetReportColumnsResult;
    }
    /**
     * Set GetReportColumnsResult value
     * @param \StructType\IntegrationResponse $getReportColumnsResult
     * @return \StructType\GetReportColumnsResponse
     */
    public function setGetReportColumnsResult(?\StructType\IntegrationResponse $getReportColumnsResult = null): self
    {
        $this->GetReportColumnsResult = $getReportColumnsResult;
        
        return $this;
    }
}
