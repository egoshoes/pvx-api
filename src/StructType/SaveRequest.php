<?php

declare(strict_types=1);

namespace StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for SaveRequest StructType
 * @subpackage Structs
 */
class SaveRequest extends AbstractStructBase
{
    /**
     * The Action
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     * @var int
     */
    protected int $Action;
    /**
     * The TemplateName
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $TemplateName = null;
    /**
     * The CsvData
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $CsvData = null;
    /**
     * Constructor method for SaveRequest
     * @uses SaveRequest::setAction()
     * @uses SaveRequest::setTemplateName()
     * @uses SaveRequest::setCsvData()
     * @param int $action
     * @param string $templateName
     * @param string $csvData
     */
    public function __construct(int $action, ?string $templateName = null, ?string $csvData = null)
    {
        $this
            ->setAction($action)
            ->setTemplateName($templateName)
            ->setCsvData($csvData);
    }
    /**
     * Get Action value
     * @return int
     */
    public function getAction(): int
    {
        return $this->Action;
    }
    /**
     * Set Action value
     * @param int $action
     * @return \StructType\SaveRequest
     */
    public function setAction(int $action): self
    {
        // validation for constraint: int
        if (!is_null($action) && !(is_int($action) || ctype_digit($action))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($action, true), gettype($action)), __LINE__);
        }
        $this->Action = $action;
        
        return $this;
    }
    /**
     * Get TemplateName value
     * @return string|null
     */
    public function getTemplateName(): ?string
    {
        return $this->TemplateName;
    }
    /**
     * Set TemplateName value
     * @param string $templateName
     * @return \StructType\SaveRequest
     */
    public function setTemplateName(?string $templateName = null): self
    {
        // validation for constraint: string
        if (!is_null($templateName) && !is_string($templateName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($templateName, true), gettype($templateName)), __LINE__);
        }
        $this->TemplateName = $templateName;
        
        return $this;
    }
    /**
     * Get CsvData value
     * @return string|null
     */
    public function getCsvData(): ?string
    {
        return $this->CsvData;
    }
    /**
     * Set CsvData value
     * @param string $csvData
     * @return \StructType\SaveRequest
     */
    public function setCsvData(?string $csvData = null): self
    {
        // validation for constraint: string
        if (!is_null($csvData) && !is_string($csvData)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($csvData, true), gettype($csvData)), __LINE__);
        }
        $this->CsvData = $csvData;
        
        return $this;
    }
}
