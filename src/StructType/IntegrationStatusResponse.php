<?php

declare(strict_types=1);

namespace StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for IntegrationStatusResponse StructType
 * @subpackage Structs
 */
class IntegrationStatusResponse extends AbstractStructBase
{
    /**
     * The Status
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     * @var string
     */
    protected string $Status;
    /**
     * The LineNo
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     * @var int
     */
    protected int $LineNo;
    /**
     * The Reference
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $Reference = null;
    /**
     * The Details
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $Details = null;
    /**
     * Constructor method for IntegrationStatusResponse
     * @uses IntegrationStatusResponse::setStatus()
     * @uses IntegrationStatusResponse::setLineNo()
     * @uses IntegrationStatusResponse::setReference()
     * @uses IntegrationStatusResponse::setDetails()
     * @param string $status
     * @param int $lineNo
     * @param string $reference
     * @param string $details
     */
    public function __construct(string $status, int $lineNo, ?string $reference = null, ?string $details = null)
    {
        $this
            ->setStatus($status)
            ->setLineNo($lineNo)
            ->setReference($reference)
            ->setDetails($details);
    }
    /**
     * Get Status value
     * @return string
     */
    public function getStatus(): string
    {
        return $this->Status;
    }
    /**
     * Set Status value
     * @uses \EnumType\IntegrationResponseStatuses::valueIsValid()
     * @uses \EnumType\IntegrationResponseStatuses::getValidValues()
     * @throws InvalidArgumentException
     * @param string $status
     * @return \StructType\IntegrationStatusResponse
     */
    public function setStatus(string $status): self
    {
        // validation for constraint: enumeration
        if (!\EnumType\IntegrationResponseStatuses::valueIsValid($status)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \EnumType\IntegrationResponseStatuses', is_array($status) ? implode(', ', $status) : var_export($status, true), implode(', ', \EnumType\IntegrationResponseStatuses::getValidValues())), __LINE__);
        }
        $this->Status = $status;
        
        return $this;
    }
    /**
     * Get LineNo value
     * @return int
     */
    public function getLineNo(): int
    {
        return $this->LineNo;
    }
    /**
     * Set LineNo value
     * @param int $lineNo
     * @return \StructType\IntegrationStatusResponse
     */
    public function setLineNo(int $lineNo): self
    {
        // validation for constraint: int
        if (!is_null($lineNo) && !(is_int($lineNo) || ctype_digit($lineNo))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($lineNo, true), gettype($lineNo)), __LINE__);
        }
        $this->LineNo = $lineNo;
        
        return $this;
    }
    /**
     * Get Reference value
     * @return string|null
     */
    public function getReference(): ?string
    {
        return $this->Reference;
    }
    /**
     * Set Reference value
     * @param string $reference
     * @return \StructType\IntegrationStatusResponse
     */
    public function setReference(?string $reference = null): self
    {
        // validation for constraint: string
        if (!is_null($reference) && !is_string($reference)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($reference, true), gettype($reference)), __LINE__);
        }
        $this->Reference = $reference;
        
        return $this;
    }
    /**
     * Get Details value
     * @return string|null
     */
    public function getDetails(): ?string
    {
        return $this->Details;
    }
    /**
     * Set Details value
     * @param string $details
     * @return \StructType\IntegrationStatusResponse
     */
    public function setDetails(?string $details = null): self
    {
        // validation for constraint: string
        if (!is_null($details) && !is_string($details)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($details, true), gettype($details)), __LINE__);
        }
        $this->Details = $details;
        
        return $this;
    }
}
