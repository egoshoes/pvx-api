<?php

declare(strict_types=1);

namespace StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for SaveData StructType
 * @subpackage Structs
 */
class SaveData extends AbstractStructBase
{
    /**
     * The saveRequest
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \StructType\SaveRequest|null
     */
    protected ?\StructType\SaveRequest $saveRequest = null;
    /**
     * Constructor method for SaveData
     * @uses SaveData::setSaveRequest()
     * @param \StructType\SaveRequest $saveRequest
     */
    public function __construct(?\StructType\SaveRequest $saveRequest = null)
    {
        $this
            ->setSaveRequest($saveRequest);
    }
    /**
     * Get saveRequest value
     * @return \StructType\SaveRequest|null
     */
    public function getSaveRequest(): ?\StructType\SaveRequest
    {
        return $this->saveRequest;
    }
    /**
     * Set saveRequest value
     * @param \StructType\SaveRequest $saveRequest
     * @return \StructType\SaveData
     */
    public function setSaveRequest(?\StructType\SaveRequest $saveRequest = null): self
    {
        $this->saveRequest = $saveRequest;
        
        return $this;
    }
}
