<?php

declare(strict_types=1);

namespace StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for SubscribeEventWithSitesFilters StructType
 * @subpackage Structs
 */
class SubscribeEventWithSitesFilters extends AbstractStructBase
{
    /**
     * The eventType
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     * @var string
     */
    protected string $eventType;
    /**
     * The filter
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $filter = null;
    /**
     * The sitesFilter
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $sitesFilter = null;
    /**
     * The callbackUrl
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $callbackUrl = null;
    /**
     * The encodeParameterData
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     * - nillable: true
     * @var bool
     */
    protected ?bool $encodeParameterData;
    /**
     * Constructor method for SubscribeEventWithSitesFilters
     * @uses SubscribeEventWithSitesFilters::setEventType()
     * @uses SubscribeEventWithSitesFilters::setFilter()
     * @uses SubscribeEventWithSitesFilters::setSitesFilter()
     * @uses SubscribeEventWithSitesFilters::setCallbackUrl()
     * @uses SubscribeEventWithSitesFilters::setEncodeParameterData()
     * @param string $eventType
     * @param string $filter
     * @param string $sitesFilter
     * @param string $callbackUrl
     * @param bool $encodeParameterData
     */
    public function __construct(string $eventType, ?string $filter = null, ?string $sitesFilter = null, ?string $callbackUrl = null, ?bool $encodeParameterData)
    {
        $this
            ->setEventType($eventType)
            ->setFilter($filter)
            ->setSitesFilter($sitesFilter)
            ->setCallbackUrl($callbackUrl)
            ->setEncodeParameterData($encodeParameterData);
    }
    /**
     * Get eventType value
     * @return string
     */
    public function getEventType(): string
    {
        return $this->eventType;
    }
    /**
     * Set eventType value
     * @uses \EnumType\SubscriptionEventTypes::valueIsValid()
     * @uses \EnumType\SubscriptionEventTypes::getValidValues()
     * @throws InvalidArgumentException
     * @param string $eventType
     * @return \StructType\SubscribeEventWithSitesFilters
     */
    public function setEventType(string $eventType): self
    {
        // validation for constraint: enumeration
        if (!\EnumType\SubscriptionEventTypes::valueIsValid($eventType)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \EnumType\SubscriptionEventTypes', is_array($eventType) ? implode(', ', $eventType) : var_export($eventType, true), implode(', ', \EnumType\SubscriptionEventTypes::getValidValues())), __LINE__);
        }
        $this->eventType = $eventType;
        
        return $this;
    }
    /**
     * Get filter value
     * @return string|null
     */
    public function getFilter(): ?string
    {
        return $this->filter;
    }
    /**
     * Set filter value
     * @param string $filter
     * @return \StructType\SubscribeEventWithSitesFilters
     */
    public function setFilter(?string $filter = null): self
    {
        // validation for constraint: string
        if (!is_null($filter) && !is_string($filter)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($filter, true), gettype($filter)), __LINE__);
        }
        $this->filter = $filter;
        
        return $this;
    }
    /**
     * Get sitesFilter value
     * @return string|null
     */
    public function getSitesFilter(): ?string
    {
        return $this->sitesFilter;
    }
    /**
     * Set sitesFilter value
     * @param string $sitesFilter
     * @return \StructType\SubscribeEventWithSitesFilters
     */
    public function setSitesFilter(?string $sitesFilter = null): self
    {
        // validation for constraint: string
        if (!is_null($sitesFilter) && !is_string($sitesFilter)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($sitesFilter, true), gettype($sitesFilter)), __LINE__);
        }
        $this->sitesFilter = $sitesFilter;
        
        return $this;
    }
    /**
     * Get callbackUrl value
     * @return string|null
     */
    public function getCallbackUrl(): ?string
    {
        return $this->callbackUrl;
    }
    /**
     * Set callbackUrl value
     * @param string $callbackUrl
     * @return \StructType\SubscribeEventWithSitesFilters
     */
    public function setCallbackUrl(?string $callbackUrl = null): self
    {
        // validation for constraint: string
        if (!is_null($callbackUrl) && !is_string($callbackUrl)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($callbackUrl, true), gettype($callbackUrl)), __LINE__);
        }
        $this->callbackUrl = $callbackUrl;
        
        return $this;
    }
    /**
     * Get encodeParameterData value
     * @return bool
     */
    public function getEncodeParameterData(): bool
    {
        return $this->encodeParameterData;
    }
    /**
     * Set encodeParameterData value
     * @param bool $encodeParameterData
     * @return \StructType\SubscribeEventWithSitesFilters
     */
    public function setEncodeParameterData(?bool $encodeParameterData): self
    {
        // validation for constraint: boolean
        if (!is_null($encodeParameterData) && !is_bool($encodeParameterData)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($encodeParameterData, true), gettype($encodeParameterData)), __LINE__);
        }
        $this->encodeParameterData = $encodeParameterData;
        
        return $this;
    }
}
