<?php

declare(strict_types=1);

namespace StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for UserSessionCredentials StructType
 * Meta information extracted from the WSDL
 * - type: tns:UserSessionCredentials
 * @subpackage Structs
 */
class UserSessionCredentials extends AbstractStructBase
{
    /**
     * The UserId
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     * - nillable: true
     * @var int
     */
    protected ?int $UserId;
    /**
     * The ClientId
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $ClientId = null;
    /**
     * The SessionId
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $SessionId = null;
    /**
     * Constructor method for UserSessionCredentials
     * @uses UserSessionCredentials::setUserId()
     * @uses UserSessionCredentials::setClientId()
     * @uses UserSessionCredentials::setSessionId()
     * @param int $userId
     * @param string $clientId
     * @param string $sessionId
     */
    public function __construct(?int $userId, ?string $clientId = null, ?string $sessionId = null)
    {
        $this
            ->setUserId($userId)
            ->setClientId($clientId)
            ->setSessionId($sessionId);
    }
    /**
     * Get UserId value
     * @return int
     */
    public function getUserId(): int
    {
        return $this->UserId;
    }
    /**
     * Set UserId value
     * @param int $userId
     * @return \StructType\UserSessionCredentials
     */
    public function setUserId(?int $userId): self
    {
        // validation for constraint: int
        if (!is_null($userId) && !(is_int($userId) || ctype_digit($userId))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($userId, true), gettype($userId)), __LINE__);
        }
        $this->UserId = $userId;
        
        return $this;
    }
    /**
     * Get ClientId value
     * @return string|null
     */
    public function getClientId(): ?string
    {
        return $this->ClientId;
    }
    /**
     * Set ClientId value
     * @param string $clientId
     * @return \StructType\UserSessionCredentials
     */
    public function setClientId(?string $clientId = null): self
    {
        // validation for constraint: string
        if (!is_null($clientId) && !is_string($clientId)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($clientId, true), gettype($clientId)), __LINE__);
        }
        $this->ClientId = $clientId;
        
        return $this;
    }
    /**
     * Get SessionId value
     * @return string|null
     */
    public function getSessionId(): ?string
    {
        return $this->SessionId;
    }
    /**
     * Set SessionId value
     * @param string $sessionId
     * @return \StructType\UserSessionCredentials
     */
    public function setSessionId(?string $sessionId = null): self
    {
        // validation for constraint: string
        if (!is_null($sessionId) && !is_string($sessionId)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($sessionId, true), gettype($sessionId)), __LINE__);
        }
        $this->SessionId = $sessionId;
        
        return $this;
    }
}
