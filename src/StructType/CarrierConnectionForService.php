<?php

declare(strict_types=1);

namespace StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for CarrierConnectionForService StructType
 * @subpackage Structs
 */
class CarrierConnectionForService extends AbstractStructBase
{
    /**
     * The Timeout
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     * @var int
     */
    protected int $Timeout;
    /**
     * The Reference
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $Reference = null;
    /**
     * The Endpoint
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $Endpoint = null;
    /**
     * The DateTimeFormat
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $DateTimeFormat = null;
    /**
     * The DocumentTypes
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \ArrayType\ArrayOfDocumentTypeMap|null
     */
    protected ?\ArrayType\ArrayOfDocumentTypeMap $DocumentTypes = null;
    /**
     * Constructor method for CarrierConnectionForService
     * @uses CarrierConnectionForService::setTimeout()
     * @uses CarrierConnectionForService::setReference()
     * @uses CarrierConnectionForService::setEndpoint()
     * @uses CarrierConnectionForService::setDateTimeFormat()
     * @uses CarrierConnectionForService::setDocumentTypes()
     * @param int $timeout
     * @param string $reference
     * @param string $endpoint
     * @param string $dateTimeFormat
     * @param \ArrayType\ArrayOfDocumentTypeMap $documentTypes
     */
    public function __construct(int $timeout, ?string $reference = null, ?string $endpoint = null, ?string $dateTimeFormat = null, ?\ArrayType\ArrayOfDocumentTypeMap $documentTypes = null)
    {
        $this
            ->setTimeout($timeout)
            ->setReference($reference)
            ->setEndpoint($endpoint)
            ->setDateTimeFormat($dateTimeFormat)
            ->setDocumentTypes($documentTypes);
    }
    /**
     * Get Timeout value
     * @return int
     */
    public function getTimeout(): int
    {
        return $this->Timeout;
    }
    /**
     * Set Timeout value
     * @param int $timeout
     * @return \StructType\CarrierConnectionForService
     */
    public function setTimeout(int $timeout): self
    {
        // validation for constraint: int
        if (!is_null($timeout) && !(is_int($timeout) || ctype_digit($timeout))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($timeout, true), gettype($timeout)), __LINE__);
        }
        $this->Timeout = $timeout;
        
        return $this;
    }
    /**
     * Get Reference value
     * @return string|null
     */
    public function getReference(): ?string
    {
        return $this->Reference;
    }
    /**
     * Set Reference value
     * @param string $reference
     * @return \StructType\CarrierConnectionForService
     */
    public function setReference(?string $reference = null): self
    {
        // validation for constraint: string
        if (!is_null($reference) && !is_string($reference)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($reference, true), gettype($reference)), __LINE__);
        }
        $this->Reference = $reference;
        
        return $this;
    }
    /**
     * Get Endpoint value
     * @return string|null
     */
    public function getEndpoint(): ?string
    {
        return $this->Endpoint;
    }
    /**
     * Set Endpoint value
     * @param string $endpoint
     * @return \StructType\CarrierConnectionForService
     */
    public function setEndpoint(?string $endpoint = null): self
    {
        // validation for constraint: string
        if (!is_null($endpoint) && !is_string($endpoint)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($endpoint, true), gettype($endpoint)), __LINE__);
        }
        $this->Endpoint = $endpoint;
        
        return $this;
    }
    /**
     * Get DateTimeFormat value
     * @return string|null
     */
    public function getDateTimeFormat(): ?string
    {
        return $this->DateTimeFormat;
    }
    /**
     * Set DateTimeFormat value
     * @param string $dateTimeFormat
     * @return \StructType\CarrierConnectionForService
     */
    public function setDateTimeFormat(?string $dateTimeFormat = null): self
    {
        // validation for constraint: string
        if (!is_null($dateTimeFormat) && !is_string($dateTimeFormat)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($dateTimeFormat, true), gettype($dateTimeFormat)), __LINE__);
        }
        $this->DateTimeFormat = $dateTimeFormat;
        
        return $this;
    }
    /**
     * Get DocumentTypes value
     * @return \ArrayType\ArrayOfDocumentTypeMap|null
     */
    public function getDocumentTypes(): ?\ArrayType\ArrayOfDocumentTypeMap
    {
        return $this->DocumentTypes;
    }
    /**
     * Set DocumentTypes value
     * @param \ArrayType\ArrayOfDocumentTypeMap $documentTypes
     * @return \StructType\CarrierConnectionForService
     */
    public function setDocumentTypes(?\ArrayType\ArrayOfDocumentTypeMap $documentTypes = null): self
    {
        $this->DocumentTypes = $documentTypes;
        
        return $this;
    }
}
