<?php

declare(strict_types=1);

namespace StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for CarrierPlatformResponse StructType
 * @subpackage Structs
 */
class CarrierPlatformResponse extends IntegrationResponse
{
    /**
     * The CarrierConnections
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \ArrayType\ArrayOfCarrierConnectionForService|null
     */
    protected ?\ArrayType\ArrayOfCarrierConnectionForService $CarrierConnections = null;
    /**
     * Constructor method for CarrierPlatformResponse
     * @uses CarrierPlatformResponse::setCarrierConnections()
     * @param \ArrayType\ArrayOfCarrierConnectionForService $carrierConnections
     */
    public function __construct(?\ArrayType\ArrayOfCarrierConnectionForService $carrierConnections = null)
    {
        $this
            ->setCarrierConnections($carrierConnections);
    }
    /**
     * Get CarrierConnections value
     * @return \ArrayType\ArrayOfCarrierConnectionForService|null
     */
    public function getCarrierConnections(): ?\ArrayType\ArrayOfCarrierConnectionForService
    {
        return $this->CarrierConnections;
    }
    /**
     * Set CarrierConnections value
     * @param \ArrayType\ArrayOfCarrierConnectionForService $carrierConnections
     * @return \StructType\CarrierPlatformResponse
     */
    public function setCarrierConnections(?\ArrayType\ArrayOfCarrierConnectionForService $carrierConnections = null): self
    {
        $this->CarrierConnections = $carrierConnections;
        
        return $this;
    }
}
