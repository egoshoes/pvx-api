<?php

declare(strict_types=1);

namespace StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for UnsubscribeEvent StructType
 * @subpackage Structs
 */
class UnsubscribeEvent extends AbstractStructBase
{
    /**
     * The subscriptionId
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     * @var int
     */
    protected int $subscriptionId;
    /**
     * Constructor method for UnsubscribeEvent
     * @uses UnsubscribeEvent::setSubscriptionId()
     * @param int $subscriptionId
     */
    public function __construct(int $subscriptionId)
    {
        $this
            ->setSubscriptionId($subscriptionId);
    }
    /**
     * Get subscriptionId value
     * @return int
     */
    public function getSubscriptionId(): int
    {
        return $this->subscriptionId;
    }
    /**
     * Set subscriptionId value
     * @param int $subscriptionId
     * @return \StructType\UnsubscribeEvent
     */
    public function setSubscriptionId(int $subscriptionId): self
    {
        // validation for constraint: int
        if (!is_null($subscriptionId) && !(is_int($subscriptionId) || ctype_digit($subscriptionId))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($subscriptionId, true), gettype($subscriptionId)), __LINE__);
        }
        $this->subscriptionId = $subscriptionId;
        
        return $this;
    }
}
