<?php

declare(strict_types=1);

namespace StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for SubscribePostEventResponse StructType
 * @subpackage Structs
 */
class SubscribePostEventResponse extends AbstractStructBase
{
    /**
     * The SubscribePostEventResult
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \StructType\IntegrationResponse|null
     */
    protected ?\StructType\IntegrationResponse $SubscribePostEventResult = null;
    /**
     * Constructor method for SubscribePostEventResponse
     * @uses SubscribePostEventResponse::setSubscribePostEventResult()
     * @param \StructType\IntegrationResponse $subscribePostEventResult
     */
    public function __construct(?\StructType\IntegrationResponse $subscribePostEventResult = null)
    {
        $this
            ->setSubscribePostEventResult($subscribePostEventResult);
    }
    /**
     * Get SubscribePostEventResult value
     * @return \StructType\IntegrationResponse|null
     */
    public function getSubscribePostEventResult(): ?\StructType\IntegrationResponse
    {
        return $this->SubscribePostEventResult;
    }
    /**
     * Set SubscribePostEventResult value
     * @param \StructType\IntegrationResponse $subscribePostEventResult
     * @return \StructType\SubscribePostEventResponse
     */
    public function setSubscribePostEventResult(?\StructType\IntegrationResponse $subscribePostEventResult = null): self
    {
        $this->SubscribePostEventResult = $subscribePostEventResult;
        
        return $this;
    }
}
