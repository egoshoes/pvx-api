<?php

declare(strict_types=1);

namespace StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetRequest StructType
 * @subpackage Structs
 */
class GetRequest extends AbstractStructBase
{
    /**
     * The PageNo
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     * @var int
     */
    protected int $PageNo;
    /**
     * The ItemsPerPage
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     * @var int
     */
    protected int $ItemsPerPage;
    /**
     * The TemplateName
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $TemplateName = null;
    /**
     * The SearchClause
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $SearchClause = null;
    /**
     * Constructor method for GetRequest
     * @uses GetRequest::setPageNo()
     * @uses GetRequest::setItemsPerPage()
     * @uses GetRequest::setTemplateName()
     * @uses GetRequest::setSearchClause()
     * @param int $pageNo
     * @param int $itemsPerPage
     * @param string $templateName
     * @param string $searchClause
     */
    public function __construct(int $pageNo, int $itemsPerPage, ?string $templateName = null, ?string $searchClause = null)
    {
        $this
            ->setPageNo($pageNo)
            ->setItemsPerPage($itemsPerPage)
            ->setTemplateName($templateName)
            ->setSearchClause($searchClause);
    }
    /**
     * Get PageNo value
     * @return int
     */
    public function getPageNo(): int
    {
        return $this->PageNo;
    }
    /**
     * Set PageNo value
     * @param int $pageNo
     * @return \StructType\GetRequest
     */
    public function setPageNo(int $pageNo): self
    {
        // validation for constraint: int
        if (!is_null($pageNo) && !(is_int($pageNo) || ctype_digit($pageNo))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($pageNo, true), gettype($pageNo)), __LINE__);
        }
        $this->PageNo = $pageNo;
        
        return $this;
    }
    /**
     * Get ItemsPerPage value
     * @return int
     */
    public function getItemsPerPage(): int
    {
        return $this->ItemsPerPage;
    }
    /**
     * Set ItemsPerPage value
     * @param int $itemsPerPage
     * @return \StructType\GetRequest
     */
    public function setItemsPerPage(int $itemsPerPage): self
    {
        // validation for constraint: int
        if (!is_null($itemsPerPage) && !(is_int($itemsPerPage) || ctype_digit($itemsPerPage))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($itemsPerPage, true), gettype($itemsPerPage)), __LINE__);
        }
        $this->ItemsPerPage = $itemsPerPage;
        
        return $this;
    }
    /**
     * Get TemplateName value
     * @return string|null
     */
    public function getTemplateName(): ?string
    {
        return $this->TemplateName;
    }
    /**
     * Set TemplateName value
     * @param string $templateName
     * @return \StructType\GetRequest
     */
    public function setTemplateName(?string $templateName = null): self
    {
        // validation for constraint: string
        if (!is_null($templateName) && !is_string($templateName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($templateName, true), gettype($templateName)), __LINE__);
        }
        $this->TemplateName = $templateName;
        
        return $this;
    }
    /**
     * Get SearchClause value
     * @return string|null
     */
    public function getSearchClause(): ?string
    {
        return $this->SearchClause;
    }
    /**
     * Set SearchClause value
     * @param string $searchClause
     * @return \StructType\GetRequest
     */
    public function setSearchClause(?string $searchClause = null): self
    {
        // validation for constraint: string
        if (!is_null($searchClause) && !is_string($searchClause)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($searchClause, true), gettype($searchClause)), __LINE__);
        }
        $this->SearchClause = $searchClause;
        
        return $this;
    }
}
