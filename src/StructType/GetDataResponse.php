<?php

declare(strict_types=1);

namespace StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetDataResponse StructType
 * @subpackage Structs
 */
class GetDataResponse extends AbstractStructBase
{
    /**
     * The GetDataResult
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \StructType\IntegrationResponse|null
     */
    protected ?\StructType\IntegrationResponse $GetDataResult = null;
    /**
     * Constructor method for GetDataResponse
     * @uses GetDataResponse::setGetDataResult()
     * @param \StructType\IntegrationResponse $getDataResult
     */
    public function __construct(?\StructType\IntegrationResponse $getDataResult = null)
    {
        $this
            ->setGetDataResult($getDataResult);
    }
    /**
     * Get GetDataResult value
     * @return \StructType\IntegrationResponse|null
     */
    public function getGetDataResult(): ?\StructType\IntegrationResponse
    {
        return $this->GetDataResult;
    }
    /**
     * Set GetDataResult value
     * @param \StructType\IntegrationResponse $getDataResult
     * @return \StructType\GetDataResponse
     */
    public function setGetDataResult(?\StructType\IntegrationResponse $getDataResult = null): self
    {
        $this->GetDataResult = $getDataResult;
        
        return $this;
    }
}
