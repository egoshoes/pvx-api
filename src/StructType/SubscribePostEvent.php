<?php

declare(strict_types=1);

namespace StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for SubscribePostEvent StructType
 * @subpackage Structs
 */
class SubscribePostEvent extends AbstractStructBase
{
    /**
     * The eventType
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     * @var string
     */
    protected string $eventType;
    /**
     * The filter
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $filter = null;
    /**
     * The postUrl
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $postUrl = null;
    /**
     * The postParams
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $postParams = null;
    /**
     * The encodeParameterData
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     * - nillable: true
     * @var bool
     */
    protected ?bool $encodeParameterData;
    /**
     * Constructor method for SubscribePostEvent
     * @uses SubscribePostEvent::setEventType()
     * @uses SubscribePostEvent::setFilter()
     * @uses SubscribePostEvent::setPostUrl()
     * @uses SubscribePostEvent::setPostParams()
     * @uses SubscribePostEvent::setEncodeParameterData()
     * @param string $eventType
     * @param string $filter
     * @param string $postUrl
     * @param string $postParams
     * @param bool $encodeParameterData
     */
    public function __construct(string $eventType, ?string $filter = null, ?string $postUrl = null, ?string $postParams = null, ?bool $encodeParameterData)
    {
        $this
            ->setEventType($eventType)
            ->setFilter($filter)
            ->setPostUrl($postUrl)
            ->setPostParams($postParams)
            ->setEncodeParameterData($encodeParameterData);
    }
    /**
     * Get eventType value
     * @return string
     */
    public function getEventType(): string
    {
        return $this->eventType;
    }
    /**
     * Set eventType value
     * @uses \EnumType\SubscriptionEventTypes::valueIsValid()
     * @uses \EnumType\SubscriptionEventTypes::getValidValues()
     * @throws InvalidArgumentException
     * @param string $eventType
     * @return \StructType\SubscribePostEvent
     */
    public function setEventType(string $eventType): self
    {
        // validation for constraint: enumeration
        if (!\EnumType\SubscriptionEventTypes::valueIsValid($eventType)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \EnumType\SubscriptionEventTypes', is_array($eventType) ? implode(', ', $eventType) : var_export($eventType, true), implode(', ', \EnumType\SubscriptionEventTypes::getValidValues())), __LINE__);
        }
        $this->eventType = $eventType;
        
        return $this;
    }
    /**
     * Get filter value
     * @return string|null
     */
    public function getFilter(): ?string
    {
        return $this->filter;
    }
    /**
     * Set filter value
     * @param string $filter
     * @return \StructType\SubscribePostEvent
     */
    public function setFilter(?string $filter = null): self
    {
        // validation for constraint: string
        if (!is_null($filter) && !is_string($filter)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($filter, true), gettype($filter)), __LINE__);
        }
        $this->filter = $filter;
        
        return $this;
    }
    /**
     * Get postUrl value
     * @return string|null
     */
    public function getPostUrl(): ?string
    {
        return $this->postUrl;
    }
    /**
     * Set postUrl value
     * @param string $postUrl
     * @return \StructType\SubscribePostEvent
     */
    public function setPostUrl(?string $postUrl = null): self
    {
        // validation for constraint: string
        if (!is_null($postUrl) && !is_string($postUrl)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($postUrl, true), gettype($postUrl)), __LINE__);
        }
        $this->postUrl = $postUrl;
        
        return $this;
    }
    /**
     * Get postParams value
     * @return string|null
     */
    public function getPostParams(): ?string
    {
        return $this->postParams;
    }
    /**
     * Set postParams value
     * @param string $postParams
     * @return \StructType\SubscribePostEvent
     */
    public function setPostParams(?string $postParams = null): self
    {
        // validation for constraint: string
        if (!is_null($postParams) && !is_string($postParams)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($postParams, true), gettype($postParams)), __LINE__);
        }
        $this->postParams = $postParams;
        
        return $this;
    }
    /**
     * Get encodeParameterData value
     * @return bool
     */
    public function getEncodeParameterData(): bool
    {
        return $this->encodeParameterData;
    }
    /**
     * Set encodeParameterData value
     * @param bool $encodeParameterData
     * @return \StructType\SubscribePostEvent
     */
    public function setEncodeParameterData(?bool $encodeParameterData): self
    {
        // validation for constraint: boolean
        if (!is_null($encodeParameterData) && !is_bool($encodeParameterData)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($encodeParameterData, true), gettype($encodeParameterData)), __LINE__);
        }
        $this->encodeParameterData = $encodeParameterData;
        
        return $this;
    }
}
