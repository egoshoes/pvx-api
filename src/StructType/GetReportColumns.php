<?php

declare(strict_types=1);

namespace StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetReportColumns StructType
 * @subpackage Structs
 */
class GetReportColumns extends AbstractStructBase
{
    /**
     * The templateName
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $templateName = null;
    /**
     * Constructor method for GetReportColumns
     * @uses GetReportColumns::setTemplateName()
     * @param string $templateName
     */
    public function __construct(?string $templateName = null)
    {
        $this
            ->setTemplateName($templateName);
    }
    /**
     * Get templateName value
     * @return string|null
     */
    public function getTemplateName(): ?string
    {
        return $this->templateName;
    }
    /**
     * Set templateName value
     * @param string $templateName
     * @return \StructType\GetReportColumns
     */
    public function setTemplateName(?string $templateName = null): self
    {
        // validation for constraint: string
        if (!is_null($templateName) && !is_string($templateName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($templateName, true), gettype($templateName)), __LINE__);
        }
        $this->templateName = $templateName;
        
        return $this;
    }
}
