<?php

declare(strict_types=1);

namespace StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetData StructType
 * @subpackage Structs
 */
class GetData extends AbstractStructBase
{
    /**
     * The getRequest
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \StructType\GetRequest|null
     */
    protected ?\StructType\GetRequest $getRequest = null;
    /**
     * Constructor method for GetData
     * @uses GetData::setGetRequest()
     * @param \StructType\GetRequest $getRequest
     */
    public function __construct(?\StructType\GetRequest $getRequest = null)
    {
        $this
            ->setGetRequest($getRequest);
    }
    /**
     * Get getRequest value
     * @return \StructType\GetRequest|null
     */
    public function getGetRequest(): ?\StructType\GetRequest
    {
        return $this->getRequest;
    }
    /**
     * Set getRequest value
     * @param \StructType\GetRequest $getRequest
     * @return \StructType\GetData
     */
    public function setGetRequest(?\StructType\GetRequest $getRequest = null): self
    {
        $this->getRequest = $getRequest;
        
        return $this;
    }
}
