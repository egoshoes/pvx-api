<?php

declare(strict_types=1);

namespace StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetSaveTemplateResponse StructType
 * @subpackage Structs
 */
class GetSaveTemplateResponse extends AbstractStructBase
{
    /**
     * The GetSaveTemplateResult
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \StructType\IntegrationResponse|null
     */
    protected ?\StructType\IntegrationResponse $GetSaveTemplateResult = null;
    /**
     * Constructor method for GetSaveTemplateResponse
     * @uses GetSaveTemplateResponse::setGetSaveTemplateResult()
     * @param \StructType\IntegrationResponse $getSaveTemplateResult
     */
    public function __construct(?\StructType\IntegrationResponse $getSaveTemplateResult = null)
    {
        $this
            ->setGetSaveTemplateResult($getSaveTemplateResult);
    }
    /**
     * Get GetSaveTemplateResult value
     * @return \StructType\IntegrationResponse|null
     */
    public function getGetSaveTemplateResult(): ?\StructType\IntegrationResponse
    {
        return $this->GetSaveTemplateResult;
    }
    /**
     * Set GetSaveTemplateResult value
     * @param \StructType\IntegrationResponse $getSaveTemplateResult
     * @return \StructType\GetSaveTemplateResponse
     */
    public function setGetSaveTemplateResult(?\StructType\IntegrationResponse $getSaveTemplateResult = null): self
    {
        $this->GetSaveTemplateResult = $getSaveTemplateResult;
        
        return $this;
    }
}
