<?php

declare(strict_types=1);

namespace StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for IntegrationResponse StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:IntegrationResponse
 * @subpackage Structs
 */
class IntegrationResponse extends AbstractStructBase
{
    /**
     * The ResponseId
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     * @var int
     */
    protected int $ResponseId;
    /**
     * The TotalCount
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     * @var int
     */
    protected int $TotalCount;
    /**
     * The ImportingQueueId
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     * @var int
     */
    protected int $ImportingQueueId;
    /**
     * The Detail
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $Detail = null;
    /**
     * The Statuses
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \ArrayType\ArrayOfIntegrationStatusResponse|null
     */
    protected ?\ArrayType\ArrayOfIntegrationStatusResponse $Statuses = null;
    /**
     * The SalesOrdersToDespatchIds
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \ArrayType\ArrayOfInt|null
     */
    protected ?\ArrayType\ArrayOfInt $SalesOrdersToDespatchIds = null;
    /**
     * The ErrorCode
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $ErrorCode = null;
    /**
     * Constructor method for IntegrationResponse
     * @uses IntegrationResponse::setResponseId()
     * @uses IntegrationResponse::setTotalCount()
     * @uses IntegrationResponse::setImportingQueueId()
     * @uses IntegrationResponse::setDetail()
     * @uses IntegrationResponse::setStatuses()
     * @uses IntegrationResponse::setSalesOrdersToDespatchIds()
     * @uses IntegrationResponse::setErrorCode()
     * @param int $responseId
     * @param int $totalCount
     * @param int $importingQueueId
     * @param string $detail
     * @param \ArrayType\ArrayOfIntegrationStatusResponse $statuses
     * @param \ArrayType\ArrayOfInt $salesOrdersToDespatchIds
     * @param string $errorCode
     */
    public function __construct(int $responseId, int $totalCount, int $importingQueueId, ?string $detail = null, ?\ArrayType\ArrayOfIntegrationStatusResponse $statuses = null, ?\ArrayType\ArrayOfInt $salesOrdersToDespatchIds = null, ?string $errorCode = null)
    {
        $this
            ->setResponseId($responseId)
            ->setTotalCount($totalCount)
            ->setImportingQueueId($importingQueueId)
            ->setDetail($detail)
            ->setStatuses($statuses)
            ->setSalesOrdersToDespatchIds($salesOrdersToDespatchIds)
            ->setErrorCode($errorCode);
    }
    /**
     * Get ResponseId value
     * @return int
     */
    public function getResponseId(): int
    {
        return $this->ResponseId;
    }
    /**
     * Set ResponseId value
     * @param int $responseId
     * @return \StructType\IntegrationResponse
     */
    public function setResponseId(int $responseId): self
    {
        // validation for constraint: int
        if (!is_null($responseId) && !(is_int($responseId) || ctype_digit($responseId))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($responseId, true), gettype($responseId)), __LINE__);
        }
        $this->ResponseId = $responseId;
        
        return $this;
    }
    /**
     * Get TotalCount value
     * @return int
     */
    public function getTotalCount(): int
    {
        return $this->TotalCount;
    }
    /**
     * Set TotalCount value
     * @param int $totalCount
     * @return \StructType\IntegrationResponse
     */
    public function setTotalCount(int $totalCount): self
    {
        // validation for constraint: int
        if (!is_null($totalCount) && !(is_int($totalCount) || ctype_digit($totalCount))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($totalCount, true), gettype($totalCount)), __LINE__);
        }
        $this->TotalCount = $totalCount;
        
        return $this;
    }
    /**
     * Get ImportingQueueId value
     * @return int
     */
    public function getImportingQueueId(): int
    {
        return $this->ImportingQueueId;
    }
    /**
     * Set ImportingQueueId value
     * @param int $importingQueueId
     * @return \StructType\IntegrationResponse
     */
    public function setImportingQueueId(int $importingQueueId): self
    {
        // validation for constraint: int
        if (!is_null($importingQueueId) && !(is_int($importingQueueId) || ctype_digit($importingQueueId))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($importingQueueId, true), gettype($importingQueueId)), __LINE__);
        }
        $this->ImportingQueueId = $importingQueueId;
        
        return $this;
    }
    /**
     * Get Detail value
     * @return string|null
     */
    public function getDetail(): ?string
    {
        return $this->Detail;
    }
    /**
     * Set Detail value
     * @param string $detail
     * @return \StructType\IntegrationResponse
     */
    public function setDetail(?string $detail = null): self
    {
        // validation for constraint: string
        if (!is_null($detail) && !is_string($detail)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($detail, true), gettype($detail)), __LINE__);
        }
        $this->Detail = $detail;
        
        return $this;
    }
    /**
     * Get Statuses value
     * @return \ArrayType\ArrayOfIntegrationStatusResponse|null
     */
    public function getStatuses(): ?\ArrayType\ArrayOfIntegrationStatusResponse
    {
        return $this->Statuses;
    }
    /**
     * Set Statuses value
     * @param \ArrayType\ArrayOfIntegrationStatusResponse $statuses
     * @return \StructType\IntegrationResponse
     */
    public function setStatuses(?\ArrayType\ArrayOfIntegrationStatusResponse $statuses = null): self
    {
        $this->Statuses = $statuses;
        
        return $this;
    }
    /**
     * Get SalesOrdersToDespatchIds value
     * @return \ArrayType\ArrayOfInt|null
     */
    public function getSalesOrdersToDespatchIds(): ?\ArrayType\ArrayOfInt
    {
        return $this->SalesOrdersToDespatchIds;
    }
    /**
     * Set SalesOrdersToDespatchIds value
     * @param \ArrayType\ArrayOfInt $salesOrdersToDespatchIds
     * @return \StructType\IntegrationResponse
     */
    public function setSalesOrdersToDespatchIds(?\ArrayType\ArrayOfInt $salesOrdersToDespatchIds = null): self
    {
        $this->SalesOrdersToDespatchIds = $salesOrdersToDespatchIds;
        
        return $this;
    }
    /**
     * Get ErrorCode value
     * @return string|null
     */
    public function getErrorCode(): ?string
    {
        return $this->ErrorCode;
    }
    /**
     * Set ErrorCode value
     * @param string $errorCode
     * @return \StructType\IntegrationResponse
     */
    public function setErrorCode(?string $errorCode = null): self
    {
        // validation for constraint: string
        if (!is_null($errorCode) && !is_string($errorCode)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($errorCode, true), gettype($errorCode)), __LINE__);
        }
        $this->ErrorCode = $errorCode;
        
        return $this;
    }
}
