<?php

declare(strict_types=1);

namespace StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for SubscribeEventWithSitesFiltersResponse StructType
 * @subpackage Structs
 */
class SubscribeEventWithSitesFiltersResponse extends AbstractStructBase
{
    /**
     * The SubscribeEventWithSitesFiltersResult
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \StructType\IntegrationResponse|null
     */
    protected ?\StructType\IntegrationResponse $SubscribeEventWithSitesFiltersResult = null;
    /**
     * Constructor method for SubscribeEventWithSitesFiltersResponse
     * @uses SubscribeEventWithSitesFiltersResponse::setSubscribeEventWithSitesFiltersResult()
     * @param \StructType\IntegrationResponse $subscribeEventWithSitesFiltersResult
     */
    public function __construct(?\StructType\IntegrationResponse $subscribeEventWithSitesFiltersResult = null)
    {
        $this
            ->setSubscribeEventWithSitesFiltersResult($subscribeEventWithSitesFiltersResult);
    }
    /**
     * Get SubscribeEventWithSitesFiltersResult value
     * @return \StructType\IntegrationResponse|null
     */
    public function getSubscribeEventWithSitesFiltersResult(): ?\StructType\IntegrationResponse
    {
        return $this->SubscribeEventWithSitesFiltersResult;
    }
    /**
     * Set SubscribeEventWithSitesFiltersResult value
     * @param \StructType\IntegrationResponse $subscribeEventWithSitesFiltersResult
     * @return \StructType\SubscribeEventWithSitesFiltersResponse
     */
    public function setSubscribeEventWithSitesFiltersResult(?\StructType\IntegrationResponse $subscribeEventWithSitesFiltersResult = null): self
    {
        $this->SubscribeEventWithSitesFiltersResult = $subscribeEventWithSitesFiltersResult;
        
        return $this;
    }
}
