<?php

declare(strict_types=1);

namespace StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for AuthenticateResponse StructType
 * @subpackage Structs
 */
class AuthenticateResponse extends AbstractStructBase
{
    /**
     * The AuthenticateResult
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \StructType\IntegrationResponse|null
     */
    protected ?\StructType\IntegrationResponse $AuthenticateResult = null;
    /**
     * Constructor method for AuthenticateResponse
     * @uses AuthenticateResponse::setAuthenticateResult()
     * @param \StructType\IntegrationResponse $authenticateResult
     */
    public function __construct(?\StructType\IntegrationResponse $authenticateResult = null)
    {
        $this
            ->setAuthenticateResult($authenticateResult);
    }
    /**
     * Get AuthenticateResult value
     * @return \StructType\IntegrationResponse|null
     */
    public function getAuthenticateResult(): ?\StructType\IntegrationResponse
    {
        return $this->AuthenticateResult;
    }
    /**
     * Set AuthenticateResult value
     * @param \StructType\IntegrationResponse $authenticateResult
     * @return \StructType\AuthenticateResponse
     */
    public function setAuthenticateResult(?\StructType\IntegrationResponse $authenticateResult = null): self
    {
        $this->AuthenticateResult = $authenticateResult;
        
        return $this;
    }
}
