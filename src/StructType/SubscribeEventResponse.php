<?php

declare(strict_types=1);

namespace StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for SubscribeEventResponse StructType
 * @subpackage Structs
 */
class SubscribeEventResponse extends AbstractStructBase
{
    /**
     * The SubscribeEventResult
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \StructType\IntegrationResponse|null
     */
    protected ?\StructType\IntegrationResponse $SubscribeEventResult = null;
    /**
     * Constructor method for SubscribeEventResponse
     * @uses SubscribeEventResponse::setSubscribeEventResult()
     * @param \StructType\IntegrationResponse $subscribeEventResult
     */
    public function __construct(?\StructType\IntegrationResponse $subscribeEventResult = null)
    {
        $this
            ->setSubscribeEventResult($subscribeEventResult);
    }
    /**
     * Get SubscribeEventResult value
     * @return \StructType\IntegrationResponse|null
     */
    public function getSubscribeEventResult(): ?\StructType\IntegrationResponse
    {
        return $this->SubscribeEventResult;
    }
    /**
     * Set SubscribeEventResult value
     * @param \StructType\IntegrationResponse $subscribeEventResult
     * @return \StructType\SubscribeEventResponse
     */
    public function setSubscribeEventResult(?\StructType\IntegrationResponse $subscribeEventResult = null): self
    {
        $this->SubscribeEventResult = $subscribeEventResult;
        
        return $this;
    }
}
