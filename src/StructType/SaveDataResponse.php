<?php

declare(strict_types=1);

namespace StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for SaveDataResponse StructType
 * @subpackage Structs
 */
class SaveDataResponse extends AbstractStructBase
{
    /**
     * The SaveDataResult
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \StructType\IntegrationResponse|null
     */
    protected ?\StructType\IntegrationResponse $SaveDataResult = null;
    /**
     * Constructor method for SaveDataResponse
     * @uses SaveDataResponse::setSaveDataResult()
     * @param \StructType\IntegrationResponse $saveDataResult
     */
    public function __construct(?\StructType\IntegrationResponse $saveDataResult = null)
    {
        $this
            ->setSaveDataResult($saveDataResult);
    }
    /**
     * Get SaveDataResult value
     * @return \StructType\IntegrationResponse|null
     */
    public function getSaveDataResult(): ?\StructType\IntegrationResponse
    {
        return $this->SaveDataResult;
    }
    /**
     * Set SaveDataResult value
     * @param \StructType\IntegrationResponse $saveDataResult
     * @return \StructType\SaveDataResponse
     */
    public function setSaveDataResult(?\StructType\IntegrationResponse $saveDataResult = null): self
    {
        $this->SaveDataResult = $saveDataResult;
        
        return $this;
    }
}
