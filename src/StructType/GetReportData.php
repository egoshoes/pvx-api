<?php

declare(strict_types=1);

namespace StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetReportData StructType
 * @subpackage Structs
 */
class GetReportData extends AbstractStructBase
{
    /**
     * The getReportRequest
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \StructType\GetReportRequest|null
     */
    protected ?\StructType\GetReportRequest $getReportRequest = null;
    /**
     * Constructor method for GetReportData
     * @uses GetReportData::setGetReportRequest()
     * @param \StructType\GetReportRequest $getReportRequest
     */
    public function __construct(?\StructType\GetReportRequest $getReportRequest = null)
    {
        $this
            ->setGetReportRequest($getReportRequest);
    }
    /**
     * Get getReportRequest value
     * @return \StructType\GetReportRequest|null
     */
    public function getGetReportRequest(): ?\StructType\GetReportRequest
    {
        return $this->getReportRequest;
    }
    /**
     * Set getReportRequest value
     * @param \StructType\GetReportRequest $getReportRequest
     * @return \StructType\GetReportData
     */
    public function setGetReportRequest(?\StructType\GetReportRequest $getReportRequest = null): self
    {
        $this->getReportRequest = $getReportRequest;
        
        return $this;
    }
}
