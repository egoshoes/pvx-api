<?php

declare(strict_types=1);

namespace StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for UnsubscribeEventResponse StructType
 * @subpackage Structs
 */
class UnsubscribeEventResponse extends AbstractStructBase
{
    /**
     * The UnsubscribeEventResult
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \StructType\IntegrationResponse|null
     */
    protected ?\StructType\IntegrationResponse $UnsubscribeEventResult = null;
    /**
     * Constructor method for UnsubscribeEventResponse
     * @uses UnsubscribeEventResponse::setUnsubscribeEventResult()
     * @param \StructType\IntegrationResponse $unsubscribeEventResult
     */
    public function __construct(?\StructType\IntegrationResponse $unsubscribeEventResult = null)
    {
        $this
            ->setUnsubscribeEventResult($unsubscribeEventResult);
    }
    /**
     * Get UnsubscribeEventResult value
     * @return \StructType\IntegrationResponse|null
     */
    public function getUnsubscribeEventResult(): ?\StructType\IntegrationResponse
    {
        return $this->UnsubscribeEventResult;
    }
    /**
     * Set UnsubscribeEventResult value
     * @param \StructType\IntegrationResponse $unsubscribeEventResult
     * @return \StructType\UnsubscribeEventResponse
     */
    public function setUnsubscribeEventResult(?\StructType\IntegrationResponse $unsubscribeEventResult = null): self
    {
        $this->UnsubscribeEventResult = $unsubscribeEventResult;
        
        return $this;
    }
}
