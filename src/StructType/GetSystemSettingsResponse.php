<?php

declare(strict_types=1);

namespace StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetSystemSettingsResponse StructType
 * @subpackage Structs
 */
class GetSystemSettingsResponse extends AbstractStructBase
{
    /**
     * The GetSystemSettingsResult
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \StructType\IntegrationResponse|null
     */
    protected ?\StructType\IntegrationResponse $GetSystemSettingsResult = null;
    /**
     * Constructor method for GetSystemSettingsResponse
     * @uses GetSystemSettingsResponse::setGetSystemSettingsResult()
     * @param \StructType\IntegrationResponse $getSystemSettingsResult
     */
    public function __construct(?\StructType\IntegrationResponse $getSystemSettingsResult = null)
    {
        $this
            ->setGetSystemSettingsResult($getSystemSettingsResult);
    }
    /**
     * Get GetSystemSettingsResult value
     * @return \StructType\IntegrationResponse|null
     */
    public function getGetSystemSettingsResult(): ?\StructType\IntegrationResponse
    {
        return $this->GetSystemSettingsResult;
    }
    /**
     * Set GetSystemSettingsResult value
     * @param \StructType\IntegrationResponse $getSystemSettingsResult
     * @return \StructType\GetSystemSettingsResponse
     */
    public function setGetSystemSettingsResult(?\StructType\IntegrationResponse $getSystemSettingsResult = null): self
    {
        $this->GetSystemSettingsResult = $getSystemSettingsResult;
        
        return $this;
    }
}
