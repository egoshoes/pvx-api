<?php

declare(strict_types=1);

namespace StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetReportDataResponse StructType
 * @subpackage Structs
 */
class GetReportDataResponse extends AbstractStructBase
{
    /**
     * The GetReportDataResult
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \StructType\IntegrationResponse|null
     */
    protected ?\StructType\IntegrationResponse $GetReportDataResult = null;
    /**
     * Constructor method for GetReportDataResponse
     * @uses GetReportDataResponse::setGetReportDataResult()
     * @param \StructType\IntegrationResponse $getReportDataResult
     */
    public function __construct(?\StructType\IntegrationResponse $getReportDataResult = null)
    {
        $this
            ->setGetReportDataResult($getReportDataResult);
    }
    /**
     * Get GetReportDataResult value
     * @return \StructType\IntegrationResponse|null
     */
    public function getGetReportDataResult(): ?\StructType\IntegrationResponse
    {
        return $this->GetReportDataResult;
    }
    /**
     * Set GetReportDataResult value
     * @param \StructType\IntegrationResponse $getReportDataResult
     * @return \StructType\GetReportDataResponse
     */
    public function setGetReportDataResult(?\StructType\IntegrationResponse $getReportDataResult = null): self
    {
        $this->GetReportDataResult = $getReportDataResult;
        
        return $this;
    }
}
