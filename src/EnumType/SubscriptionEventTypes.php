<?php

declare(strict_types=1);

namespace EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for SubscriptionEventTypes EnumType
 * @subpackage Enumerations
 */
class SubscriptionEventTypes extends AbstractStructEnumBase
{
    /**
     * Constant for value 'AvailabilityChanges'
     * @return string 'AvailabilityChanges'
     */
    const VALUE_AVAILABILITY_CHANGES = 'AvailabilityChanges';
    /**
     * Constant for value 'SalesOrderStatusChanges'
     * @return string 'SalesOrderStatusChanges'
     */
    const VALUE_SALES_ORDER_STATUS_CHANGES = 'SalesOrderStatusChanges';
    /**
     * Constant for value 'GoodsReceived'
     * @return string 'GoodsReceived'
     */
    const VALUE_GOODS_RECEIVED = 'GoodsReceived';
    /**
     * Constant for value 'TrackingNumberReceived'
     * @return string 'TrackingNumberReceived'
     */
    const VALUE_TRACKING_NUMBER_RECEIVED = 'TrackingNumberReceived';
    /**
     * Constant for value 'IncrementalChanges'
     * @return string 'IncrementalChanges'
     */
    const VALUE_INCREMENTAL_CHANGES = 'IncrementalChanges';
    /**
     * Constant for value 'Returns'
     * @return string 'Returns'
     */
    const VALUE_RETURNS = 'Returns';
    /**
     * Constant for value 'DespatchPackageTrackingNumberReceived'
     * @return string 'DespatchPackageTrackingNumberReceived'
     */
    const VALUE_DESPATCH_PACKAGE_TRACKING_NUMBER_RECEIVED = 'DespatchPackageTrackingNumberReceived';
    /**
     * Constant for value 'OnDespatchOrderReceived'
     * @return string 'OnDespatchOrderReceived'
     */
    const VALUE_ON_DESPATCH_ORDER_RECEIVED = 'OnDespatchOrderReceived';
    /**
     * Constant for value 'DespatchPackageDespatched'
     * @return string 'DespatchPackageDespatched'
     */
    const VALUE_DESPATCH_PACKAGE_DESPATCHED = 'DespatchPackageDespatched';
    /**
     * Return allowed values
     * @uses self::VALUE_AVAILABILITY_CHANGES
     * @uses self::VALUE_SALES_ORDER_STATUS_CHANGES
     * @uses self::VALUE_GOODS_RECEIVED
     * @uses self::VALUE_TRACKING_NUMBER_RECEIVED
     * @uses self::VALUE_INCREMENTAL_CHANGES
     * @uses self::VALUE_RETURNS
     * @uses self::VALUE_DESPATCH_PACKAGE_TRACKING_NUMBER_RECEIVED
     * @uses self::VALUE_ON_DESPATCH_ORDER_RECEIVED
     * @uses self::VALUE_DESPATCH_PACKAGE_DESPATCHED
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_AVAILABILITY_CHANGES,
            self::VALUE_SALES_ORDER_STATUS_CHANGES,
            self::VALUE_GOODS_RECEIVED,
            self::VALUE_TRACKING_NUMBER_RECEIVED,
            self::VALUE_INCREMENTAL_CHANGES,
            self::VALUE_RETURNS,
            self::VALUE_DESPATCH_PACKAGE_TRACKING_NUMBER_RECEIVED,
            self::VALUE_ON_DESPATCH_ORDER_RECEIVED,
            self::VALUE_DESPATCH_PACKAGE_DESPATCHED,
        ];
    }
}
