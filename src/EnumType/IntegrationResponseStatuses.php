<?php

declare(strict_types=1);

namespace EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for IntegrationResponseStatuses EnumType
 * @subpackage Enumerations
 */
class IntegrationResponseStatuses extends AbstractStructEnumBase
{
    /**
     * Constant for value 'Error'
     * @return string 'Error'
     */
    const VALUE_ERROR = 'Error';
    /**
     * Constant for value 'Success'
     * @return string 'Success'
     */
    const VALUE_SUCCESS = 'Success';
    /**
     * Return allowed values
     * @uses self::VALUE_ERROR
     * @uses self::VALUE_SUCCESS
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_ERROR,
            self::VALUE_SUCCESS,
        ];
    }
}
