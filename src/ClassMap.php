<?php

declare(strict_types=1);
/**
 * Class which returns the class map definition
 */
class ClassMap
{
    /**
     * Returns the mapping between the WSDL Structs and generated Structs' classes
     * This array is sent to the \SoapClient when calling the WS
     * @return string[]
     */
    final public static function get(): array
    {
        return [
            'GetData' => '\\StructType\\GetData',
            'GetRequest' => '\\StructType\\GetRequest',
            'GetDataResponse' => '\\StructType\\GetDataResponse',
            'IntegrationResponse' => '\\StructType\\IntegrationResponse',
            'ArrayOfIntegrationStatusResponse' => '\\ArrayType\\ArrayOfIntegrationStatusResponse',
            'IntegrationStatusResponse' => '\\StructType\\IntegrationStatusResponse',
            'ArrayOfInt' => '\\ArrayType\\ArrayOfInt',
            'CarrierPlatformResponse' => '\\StructType\\CarrierPlatformResponse',
            'ArrayOfCarrierConnectionForService' => '\\ArrayType\\ArrayOfCarrierConnectionForService',
            'CarrierConnectionForService' => '\\StructType\\CarrierConnectionForService',
            'ArrayOfDocumentTypeMap' => '\\ArrayType\\ArrayOfDocumentTypeMap',
            'DocumentTypeMap' => '\\StructType\\DocumentTypeMap',
            'UserSessionCredentials' => '\\StructType\\UserSessionCredentials',
            'GetSystemSettings' => '\\StructType\\GetSystemSettings',
            'GetSystemSettingsResponse' => '\\StructType\\GetSystemSettingsResponse',
            'GetReportData' => '\\StructType\\GetReportData',
            'GetReportRequest' => '\\StructType\\GetReportRequest',
            'GetReportDataResponse' => '\\StructType\\GetReportDataResponse',
            'GetReportColumns' => '\\StructType\\GetReportColumns',
            'GetReportColumnsResponse' => '\\StructType\\GetReportColumnsResponse',
            'GetSaveTemplate' => '\\StructType\\GetSaveTemplate',
            'GetSaveTemplateResponse' => '\\StructType\\GetSaveTemplateResponse',
            'SaveData' => '\\StructType\\SaveData',
            'SaveRequest' => '\\StructType\\SaveRequest',
            'SaveDataResponse' => '\\StructType\\SaveDataResponse',
            'SubscribeEvent' => '\\StructType\\SubscribeEvent',
            'SubscribeEventResponse' => '\\StructType\\SubscribeEventResponse',
            'SubscribeEventWithSitesFilters' => '\\StructType\\SubscribeEventWithSitesFilters',
            'SubscribeEventWithSitesFiltersResponse' => '\\StructType\\SubscribeEventWithSitesFiltersResponse',
            'SubscribePostEvent' => '\\StructType\\SubscribePostEvent',
            'SubscribePostEventResponse' => '\\StructType\\SubscribePostEventResponse',
            'UnsubscribeEvent' => '\\StructType\\UnsubscribeEvent',
            'UnsubscribeEventResponse' => '\\StructType\\UnsubscribeEventResponse',
            'Authenticate' => '\\StructType\\Authenticate',
            'AuthenticateResponse' => '\\StructType\\AuthenticateResponse',
        ];
    }
}
