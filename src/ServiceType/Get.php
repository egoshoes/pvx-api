<?php

declare(strict_types=1);

namespace ServiceType;

use SoapFault;
use WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Get ServiceType
 * @subpackage Services
 */
class Get extends AbstractSoapClientBase
{
    /**
     * Sets the UserSessionCredentials SoapHeader param
     * @uses AbstractSoapClientBase::setSoapHeader()
     * @param \StructType\UserSessionCredentials $userSessionCredentials
     * @param string $namespace
     * @param bool $mustUnderstand
     * @param string $actor
     * @return \ServiceType\Get
     */
    public function setSoapHeaderUserSessionCredentials(\StructType\UserSessionCredentials $userSessionCredentials, string $namespace = 'http://www.peoplevox.net/', bool $mustUnderstand = false, ?string $actor = null): self
    {
        return $this->setSoapHeader($namespace, 'UserSessionCredentials', $userSessionCredentials, $mustUnderstand, $actor);
    }
    /**
     * Method to call the operation originally named GetData
     * Meta information extracted from the WSDL
     * - SOAPHeaderNames: UserSessionCredentials
     * - SOAPHeaderNamespaces: http://www.peoplevox.net/
     * - SOAPHeaderTypes: \StructType\UserSessionCredentials
     * - SOAPHeaders: required
     * - documentation: Export data from the PeopleVox system.
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \StructType\GetData $parameters
     * @return \StructType\GetDataResponse|bool
     */
    public function GetData(\StructType\GetData $parameters)
    {
        try {
            $this->setResult($resultGetData = $this->getSoapClient()->__soapCall('GetData', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetData;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetSystemSettings
     * Meta information extracted from the WSDL
     * - SOAPHeaderNames: UserSessionCredentials
     * - SOAPHeaderNamespaces: http://www.peoplevox.net/
     * - SOAPHeaderTypes: \StructType\UserSessionCredentials
     * - SOAPHeaders: required
     * - documentation: Export settings from PeopleVox system.
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \StructType\GetSystemSettings $parameters
     * @return \StructType\GetSystemSettingsResponse|bool
     */
    public function GetSystemSettings(\StructType\GetSystemSettings $parameters)
    {
        try {
            $this->setResult($resultGetSystemSettings = $this->getSoapClient()->__soapCall('GetSystemSettings', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetSystemSettings;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetReportData
     * Meta information extracted from the WSDL
     * - SOAPHeaderNames: UserSessionCredentials
     * - SOAPHeaderNamespaces: http://www.peoplevox.net/
     * - SOAPHeaderTypes: \StructType\UserSessionCredentials
     * - SOAPHeaders: required
     * - documentation: Export reports data from the PeopleVox system.
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \StructType\GetReportData $parameters
     * @return \StructType\GetReportDataResponse|bool
     */
    public function GetReportData(\StructType\GetReportData $parameters)
    {
        try {
            $this->setResult($resultGetReportData = $this->getSoapClient()->__soapCall('GetReportData', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetReportData;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetReportColumns
     * Meta information extracted from the WSDL
     * - SOAPHeaderNames: UserSessionCredentials
     * - SOAPHeaderNamespaces: http://www.peoplevox.net/
     * - SOAPHeaderTypes: \StructType\UserSessionCredentials
     * - SOAPHeaders: required
     * - documentation: Returns the list of columns of a report.
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \StructType\GetReportColumns $parameters
     * @return \StructType\GetReportColumnsResponse|bool
     */
    public function GetReportColumns(\StructType\GetReportColumns $parameters)
    {
        try {
            $this->setResult($resultGetReportColumns = $this->getSoapClient()->__soapCall('GetReportColumns', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetReportColumns;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetSaveTemplate
     * Meta information extracted from the WSDL
     * - SOAPHeaderNames: UserSessionCredentials
     * - SOAPHeaderNamespaces: http://www.peoplevox.net/
     * - SOAPHeaderTypes: \StructType\UserSessionCredentials
     * - SOAPHeaders: required
     * - documentation: Returns the csv template for saving informations.
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \StructType\GetSaveTemplate $parameters
     * @return \StructType\GetSaveTemplateResponse|bool
     */
    public function GetSaveTemplate(\StructType\GetSaveTemplate $parameters)
    {
        try {
            $this->setResult($resultGetSaveTemplate = $this->getSoapClient()->__soapCall('GetSaveTemplate', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetSaveTemplate;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \StructType\GetDataResponse|\StructType\GetReportColumnsResponse|\StructType\GetReportDataResponse|\StructType\GetSaveTemplateResponse|\StructType\GetSystemSettingsResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
