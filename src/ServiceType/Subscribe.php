<?php

declare(strict_types=1);

namespace ServiceType;

use SoapFault;
use WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Subscribe ServiceType
 * @subpackage Services
 */
class Subscribe extends AbstractSoapClientBase
{
    /**
     * Sets the UserSessionCredentials SoapHeader param
     * @uses AbstractSoapClientBase::setSoapHeader()
     * @param \StructType\UserSessionCredentials $userSessionCredentials
     * @param string $namespace
     * @param bool $mustUnderstand
     * @param string $actor
     * @return \ServiceType\Subscribe
     */
    public function setSoapHeaderUserSessionCredentials(\StructType\UserSessionCredentials $userSessionCredentials, string $namespace = 'http://www.peoplevox.net/', bool $mustUnderstand = false, ?string $actor = null): self
    {
        return $this->setSoapHeader($namespace, 'UserSessionCredentials', $userSessionCredentials, $mustUnderstand, $actor);
    }
    /**
     * Method to call the operation originally named SubscribeEvent
     * Meta information extracted from the WSDL
     * - SOAPHeaderNames: UserSessionCredentials
     * - SOAPHeaderNamespaces: http://www.peoplevox.net/
     * - SOAPHeaderTypes: \StructType\UserSessionCredentials
     * - SOAPHeaders: required
     * - documentation: Subscribe to PeopleVox system events.
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \StructType\SubscribeEvent $parameters
     * @return \StructType\SubscribeEventResponse|bool
     */
    public function SubscribeEvent(\StructType\SubscribeEvent $parameters)
    {
        try {
            $this->setResult($resultSubscribeEvent = $this->getSoapClient()->__soapCall('SubscribeEvent', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultSubscribeEvent;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named SubscribeEventWithSitesFilters
     * Meta information extracted from the WSDL
     * - SOAPHeaderNames: UserSessionCredentials
     * - SOAPHeaderNamespaces: http://www.peoplevox.net/
     * - SOAPHeaderTypes: \StructType\UserSessionCredentials
     * - SOAPHeaders: required
     * - documentation: Subscribe to PeopleVox system events wits sites filters.
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \StructType\SubscribeEventWithSitesFilters $parameters
     * @return \StructType\SubscribeEventWithSitesFiltersResponse|bool
     */
    public function SubscribeEventWithSitesFilters(\StructType\SubscribeEventWithSitesFilters $parameters)
    {
        try {
            $this->setResult($resultSubscribeEventWithSitesFilters = $this->getSoapClient()->__soapCall('SubscribeEventWithSitesFilters', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultSubscribeEventWithSitesFilters;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named SubscribePostEvent
     * Meta information extracted from the WSDL
     * - SOAPHeaderNames: UserSessionCredentials
     * - SOAPHeaderNamespaces: http://www.peoplevox.net/
     * - SOAPHeaderTypes: \StructType\UserSessionCredentials
     * - SOAPHeaders: required
     * - documentation: Subscribe to PeopleVox system events.
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \StructType\SubscribePostEvent $parameters
     * @return \StructType\SubscribePostEventResponse|bool
     */
    public function SubscribePostEvent(\StructType\SubscribePostEvent $parameters)
    {
        try {
            $this->setResult($resultSubscribePostEvent = $this->getSoapClient()->__soapCall('SubscribePostEvent', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultSubscribePostEvent;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \StructType\SubscribeEventResponse|\StructType\SubscribeEventWithSitesFiltersResponse|\StructType\SubscribePostEventResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
