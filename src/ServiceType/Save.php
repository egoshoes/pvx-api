<?php

declare(strict_types=1);

namespace ServiceType;

use SoapFault;
use WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Save ServiceType
 * @subpackage Services
 */
class Save extends AbstractSoapClientBase
{
    /**
     * Sets the UserSessionCredentials SoapHeader param
     * @uses AbstractSoapClientBase::setSoapHeader()
     * @param \StructType\UserSessionCredentials $userSessionCredentials
     * @param string $namespace
     * @param bool $mustUnderstand
     * @param string $actor
     * @return \ServiceType\Save
     */
    public function setSoapHeaderUserSessionCredentials(\StructType\UserSessionCredentials $userSessionCredentials, string $namespace = 'http://www.peoplevox.net/', bool $mustUnderstand = false, ?string $actor = null): self
    {
        return $this->setSoapHeader($namespace, 'UserSessionCredentials', $userSessionCredentials, $mustUnderstand, $actor);
    }
    /**
     * Method to call the operation originally named SaveData
     * Meta information extracted from the WSDL
     * - SOAPHeaderNames: UserSessionCredentials
     * - SOAPHeaderNamespaces: http://www.peoplevox.net/
     * - SOAPHeaderTypes: \StructType\UserSessionCredentials
     * - SOAPHeaders: required
     * - documentation: Import data into PeopleVox system.
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \StructType\SaveData $parameters
     * @return \StructType\SaveDataResponse|bool
     */
    public function SaveData(\StructType\SaveData $parameters)
    {
        try {
            $this->setResult($resultSaveData = $this->getSoapClient()->__soapCall('SaveData', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultSaveData;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \StructType\SaveDataResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
