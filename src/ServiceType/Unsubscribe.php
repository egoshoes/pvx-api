<?php

declare(strict_types=1);

namespace ServiceType;

use SoapFault;
use WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Unsubscribe ServiceType
 * @subpackage Services
 */
class Unsubscribe extends AbstractSoapClientBase
{
    /**
     * Sets the UserSessionCredentials SoapHeader param
     * @uses AbstractSoapClientBase::setSoapHeader()
     * @param \StructType\UserSessionCredentials $userSessionCredentials
     * @param string $namespace
     * @param bool $mustUnderstand
     * @param string $actor
     * @return \ServiceType\Unsubscribe
     */
    public function setSoapHeaderUserSessionCredentials(\StructType\UserSessionCredentials $userSessionCredentials, string $namespace = 'http://www.peoplevox.net/', bool $mustUnderstand = false, ?string $actor = null): self
    {
        return $this->setSoapHeader($namespace, 'UserSessionCredentials', $userSessionCredentials, $mustUnderstand, $actor);
    }
    /**
     * Method to call the operation originally named UnsubscribeEvent
     * Meta information extracted from the WSDL
     * - SOAPHeaderNames: UserSessionCredentials
     * - SOAPHeaderNamespaces: http://www.peoplevox.net/
     * - SOAPHeaderTypes: \StructType\UserSessionCredentials
     * - SOAPHeaders: required
     * - documentation: Unsubscribe to an event that was subscribed.
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \StructType\UnsubscribeEvent $parameters
     * @return \StructType\UnsubscribeEventResponse|bool
     */
    public function UnsubscribeEvent(\StructType\UnsubscribeEvent $parameters)
    {
        try {
            $this->setResult($resultUnsubscribeEvent = $this->getSoapClient()->__soapCall('UnsubscribeEvent', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultUnsubscribeEvent;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \StructType\UnsubscribeEventResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
