<?php

declare(strict_types=1);

namespace ServiceType;

use SoapFault;
use WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Authenticate ServiceType
 * @subpackage Services
 */
class Authenticate extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named Authenticate
     * Meta information extracted from the WSDL
     * - documentation: Authenticate user.
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \StructType\Authenticate $parameters
     * @return \StructType\AuthenticateResponse|bool
     */
    public function Authenticate(\StructType\Authenticate $parameters)
    {
        try {
            $this->setResult($resultAuthenticate = $this->getSoapClient()->__soapCall('Authenticate', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultAuthenticate;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \StructType\AuthenticateResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
